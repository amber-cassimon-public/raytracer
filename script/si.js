function format_si_prefix(value) {
    const PREPOSITIONS = ["n", "μ", "m", "", "k", "M", "G"];

    // First, find log10 of value, rounded down
    let power = Math.log10(value);

    // Next, figure out the multiple-of-3 that this power is closest to
    // (Closest in a closest-to-zero) direction
    let power_round_to_3 = Math.abs(power) - (Math.abs(power) % 3);

    // Next, convert to an index (-5 is -15, -4 is -12, ...)
    // We support -3, -2, -1, 0, 1, 2, 3
    let multiple_of_3 = power_round_to_3 / 3;
    multiple_of_3 = Math.max(-3, Math.min(multiple_of_3, 3));

    // Turn into 0-based indexing
    let prep_index = multiple_of_3 + 3;

    let value_rem = (value / Math.pow(10, power_round_to_3)).toFixed(3);

    return value_rem + " " + PREPOSITIONS[prep_index];
}