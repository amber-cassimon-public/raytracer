use crate::color::Color;
use crate::point3::Point3;
use crate::texture::Texture;

pub struct CheckerTexture
{
    white: Color,
    black: Color,
    frequency: f32,
}

impl CheckerTexture
{
    pub fn new(white: Color, black: Color, frequency: f32) -> CheckerTexture
    {
        return CheckerTexture{white, black, frequency};
    }
}

impl Default for CheckerTexture
{
    fn default() -> Self {
        return CheckerTexture {
            white: Color { e: [1.0, 1.0, 1.0] },
            black: Color { e: [0.0, 0.0, 0.0] },
            frequency: 1.0,
        };
    }
}

impl Texture for CheckerTexture
{
    fn value(&self, u: f32, v: f32, _p: Point3) -> Color
    {
        let freq_u = (self.frequency * u) % 1.0;
        let freq_v = (self.frequency * v) % 1.0;

        let top_left = (freq_u < 0.5) && (freq_v < 0.5);
        let bottom_right = (0.5 <= freq_u) && (0.5 <= freq_v);

        return if top_left || bottom_right {
            Color{e: self.white.e}
        } else {
            Color{e: self.black.e}
        };
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_sampling()
    {
        let texture = CheckerTexture::default();

        assert_eq!([1.0, 1.0, 1.0], texture.value(0.25, 0.25, Point3::origin()).e);
        assert_eq!([0.0, 0.0, 0.0], texture.value(0.75, 0.25, Point3::origin()).e);
        assert_eq!([0.0, 0.0, 0.0], texture.value(0.25, 0.75, Point3::origin()).e);
        assert_eq!([1.0, 1.0, 1.0], texture.value(0.75, 0.75, Point3::origin()).e);
    }
}