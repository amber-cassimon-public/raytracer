pub fn degrees_to_radians(degrees: f32) -> f32
{
    return (degrees / 180.0) * std::f32::consts::PI;
}