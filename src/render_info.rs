use std::fmt::{Display, Formatter};

pub struct RenderInfo
{
    pub image_width: u32,
    pub image_height: u32,
    pub samples_per_pixel: u32,
    pub initial_ray_count: u64,
    pub total_ray_count: u64,
    pub ray_gen_time: std::time::Duration,
    pub render_time: std::time::Duration,
}

impl Display for RenderInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let build_type = if cfg!(debug_assertions) {
            "Debug"
        } else {
            "Release"
        };

        return write!(f,
                      "{{
    build_type: \"{}\",
    commit_hash: \"{}\",
    image_width: {},
    image_height: {},
    samples_per_pixel: {},
    initial_ray_count: {},
    total_ray_count: {},
    ray_gen_time_s: {},
    render_time_s: {},
    note: \"\"
}}",
                      build_type,
                      env!("COMMIT_HASH"),
                      self.image_width,
                      self.image_height,
                      self.samples_per_pixel,
                      self.initial_ray_count,
                      self.total_ray_count,
                      self.ray_gen_time.as_secs_f64(),
                      self.render_time.as_secs_f64()
        );
    }
}