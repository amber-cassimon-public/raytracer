use crate::{color::Color, point3::Point3, texture::Texture};

pub struct SolidTexture
{
    albedo: Color
}

impl SolidTexture
{
    pub fn new(albedo: Color) -> SolidTexture
    {
        return SolidTexture{albedo};
    }
}

impl Texture for SolidTexture
{
    fn value(&self, _u: f32, _v: f32, _p: Point3) -> Color {
        return Color{e: self.albedo.e};
    }
}

impl Default for SolidTexture
{
    fn default() -> Self {
        return SolidTexture{albedo: Color{e: [1.0, 1.0, 1.0]}};
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_sampling()
    {
        let texture = SolidTexture::default();

        assert_eq!([1.0, 1.0, 1.0], texture.value(0.5, 0.5, Point3::origin()).e);
    }
}