use std::ops::{Add, Mul};

pub fn lerp<T, R>(x0: T, x1: R, x: f32) -> <T as Add<<R as Mul<f32>>::Output>>::Output where
    T: Mul<f32, Output = T> + Add<<R as Mul<f32>>::Output>,
    R: Mul<f32>
{
    type MulResult<A> = <A as Mul<f32>>::Output;

    let _x: f32 = f32::min(f32::max(x, 0.0), 1.0);
    let _x0: MulResult<T> = x0 * (1.0 - _x);
    let _x1: MulResult<R> = x1 * _x;

    return _x0 + _x1;
}

#[cfg(test)]
mod tests
{
    use crate::rgb_pixel::RgbPixel;
    use super::*;

    #[test]
    fn test_lerp_f32()
    {
        let x0 = 5.0;
        let x1 = 10.0;

        assert_eq!(5.0, lerp(x0, x1, 0.0));
        assert_eq!(10.0, lerp(x0, x1, 1.0));
        assert_eq!(7.5, lerp(x0, x1, 0.5));
    }

    #[test]
    fn test_lerp_rgb_pixel()
    {
        let x0 = RgbPixel::white();
        let x1 = RgbPixel::black();

        assert_eq!(RgbPixel::white(), lerp(x0, x1, 0.0));
        assert_eq!(RgbPixel::black(), lerp(x0, x1, 1.0));
        assert_eq!(RgbPixel{r: 0.5, g: 0.5, b: 0.5}, lerp(x0, x1, 0.5));

        let x2 = RgbPixel::red();
        let x3 = RgbPixel::blue();

        assert_eq!(RgbPixel::red(), lerp(x2, x3, 0.0));
        assert_eq!(RgbPixel::blue(), lerp(x2, x3, 1.0));
        assert_eq!(RgbPixel{r: 0.5, g: 0.0, b: 0.5}, lerp(x2, x3, 0.5));
    }
}