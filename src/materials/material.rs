use crate::{color::Color, hit_record::HitRecord, point3::Point3, ray::Ray};

pub trait Material
{
    fn scatter(
        &self,
        ray: &Ray,
        hit_record: &HitRecord,
        attentuation: &mut Color,
        scattered: &mut Ray
    ) -> bool;

    fn emitted(&self, u: f32, v: f32, p: &Point3) -> Color;
}

