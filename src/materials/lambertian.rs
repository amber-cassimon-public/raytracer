use std::rc::Rc;
use crate::{color::Color, hit_record::HitRecord, point3::Point3, ray::Ray,
            solid_texture::SolidTexture, texture::Texture, vec3::Vec3};
use crate::materials::material::Material;

pub struct Lambertian
{
    texture: Rc<dyn Texture>
}

impl Lambertian
{
    pub fn from_solid_color(albedo: Color) -> Lambertian
    {
        return Lambertian{texture: Rc::new(SolidTexture::new(albedo))};
    }

    pub fn from_texture(texture: Rc<dyn Texture>) -> Lambertian
    {
        return Lambertian{texture};
    }
}

impl Material for Lambertian
{
    fn scatter(&self, _ray: &Ray, hit_record: &HitRecord, attenuation: &mut Color, scattered: &mut Ray) -> bool {
        let mut scatter_direction = hit_record.normal + Vec3::random_unit_vector(None);

        if scatter_direction.near_zero()
        {
            scatter_direction = hit_record.normal;
        }

        *scattered = Ray::new(hit_record.p, scatter_direction);
        *attenuation = self.texture.value(hit_record.u, hit_record.v, hit_record.p);
        return true;
    }

    fn emitted(&self, _u: f32, _v: f32, _p: &Point3) -> Color {
        return Color{e: [0.0, 0.0, 0.0]};
    }
}
