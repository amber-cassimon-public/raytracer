use std::rc::Rc;
use crate::{color::Color, hit_record::HitRecord, materials::material::Material, point3::Point3,
            ray::Ray, solid_texture::SolidTexture, texture::Texture};

pub struct DiffuseLight
{
    texture: Rc<dyn Texture>,
}

impl DiffuseLight
{
    pub fn from_solid_color(albedo: Color) -> DiffuseLight
    {
        return DiffuseLight{texture: Rc::new(SolidTexture::new(albedo))};
    }

    pub fn from_texture(texture: Rc<dyn Texture>) -> DiffuseLight
    {
        return DiffuseLight{texture};
    }
}

impl Material for DiffuseLight
{
    fn scatter(&self, _ray: &Ray, _hit_record: &HitRecord, _attentuation: &mut Color, _scattered: &mut Ray) -> bool {
        return false;
    }

    fn emitted(&self, u: f32, v: f32, p: &Point3) -> Color {
        return self.texture.value(u, v, p.clone());
    }
}