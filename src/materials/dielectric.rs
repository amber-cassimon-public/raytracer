use rand::random;
use crate::{color::Color, hit_record::HitRecord, ray::Ray,
            vec3::{dot, unit_vector, Vec3}};
use crate::materials::material::Material;
use crate::point3::Point3;

pub struct Dielectric
{
    refraction_index: f32
}

impl Dielectric
{
    pub fn new(refraction_index: f32) -> Dielectric
    {
        return Dielectric{refraction_index};
    }

    pub fn reflectance(cosine: f32, refraction_index: f32) -> f32
    {
        let r0 = (1.0 - refraction_index) / (1.0 + refraction_index);
        let r0_squared = r0 * r0;
        return r0_squared + ((1.0 - r0_squared) * (1.0 - cosine).powf(5.0));
    }
}

impl Material for Dielectric
{
    fn scatter(&self, ray: &Ray, hit_record: &HitRecord, attentuation: &mut Color, scattered: &mut Ray) -> bool {
        *attentuation = Color{e: [1.0, 1.0, 1.0]};
        let ri =  if hit_record.front_face{1.0  / self.refraction_index} else {self.refraction_index};

        let unit_direction = unit_vector(ray.direction().clone());
        let cos_theta = f32::min(dot(-unit_direction, hit_record.normal), 1.0);
        let sin_theta = f32::sqrt(1.0 - (cos_theta * cos_theta));

        let cannot_refract = 1.0 < (ri * sin_theta);
        let random_reflectance: f32 = random();

        let direction = if cannot_refract || (random_reflectance < Self::reflectance(cos_theta, ri)) {
            Vec3::reflect(&unit_direction, &hit_record.normal)
        } else {
            Vec3::refract(&unit_direction, &hit_record.normal, ri)
        };

        *scattered = Ray::new(hit_record.p, direction);
        return true;
    }

    fn emitted(&self, _u: f32, _v: f32, _p: &Point3) -> Color {
        return Color{e: [0.0, 0.0, 0.0]};
    }
}