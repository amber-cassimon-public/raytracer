use crate::{color::Color, hit_record::HitRecord, ray::Ray, vec3::dot, vec3::unit_vector, vec3::Vec3};
use crate::materials::material::Material;
use crate::point3::Point3;

pub struct Metal
{
    albedo: Color,
    fuzz: f32
}

impl Metal
{
    pub fn new(albedo: Color, fuzz: f32) -> Metal
    {
        return Metal{albedo, fuzz};
    }
}

impl Material for Metal
{
    fn scatter(&self, ray: &Ray, hit_record: &HitRecord, attentuation: &mut Color, scattered: &mut Ray) -> bool {
        let mut reflected = Vec3::reflect(ray.direction(), &hit_record.normal);
        reflected = unit_vector(reflected) + (self.fuzz  * Vec3::random_unit_vector(None));
        *scattered = Ray::new(hit_record.p, reflected);
        *attentuation = Color{e: self.albedo.e};
        return 0.0 < dot(scattered.direction().clone(), hit_record.normal);
    }

    fn emitted(&self, _u: f32, _v: f32, _p: &Point3) -> Color {
        return Color{e: [0.0, 0.0, 0.0]};
    }
}