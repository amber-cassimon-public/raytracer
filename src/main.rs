mod vec3;
mod color;
mod ray;
mod point3;
mod hit_record;
mod interval;
mod camera;
mod render_info;
mod util;
mod materials;
mod texture;
mod checker_texture;
mod solid_texture;
mod hittables;
mod rgb_pixel;
mod framebuffer;
mod lerp;
mod ray_generator;

use std::rc::Rc;
use crate::{camera::Camera, checker_texture::CheckerTexture, color::Color, point3::Point3, vec3::Vec3};
use materials::{dielectric::Dielectric, lambertian::Lambertian, metal::Metal};
use hittables::{hittable::Hittable, hittable_list::HittableList, sphere::Sphere, triangle::Triangle};
use crate::materials::diffuse_light::DiffuseLight;
use crate::solid_texture::SolidTexture;

fn cornell_box(camera: &mut Camera) -> HittableList
{
    // First, prepare all materials
    let checkerboard = Rc::new(CheckerTexture::new(
        Color::white(),
        Color::black(),
        4.0
    ));

    let white_wall = Rc::new(Lambertian::from_solid_color(Color::white()));
    let back_wall = Rc::new(Metal::new(Color::white(), 0.5));
    let red_wall = Rc::new(Metal::new(Color::red(), 0.5));
    let green_wall = Rc::new(Metal::new(Color::green(), 0.5));
    let blue_wall = Rc::new(Lambertian::from_solid_color(Color::blue()));
    let ceiling_light = Rc::new(DiffuseLight::from_solid_color(Color::white()));
    let floor = Rc::new(Lambertian::from_texture(checkerboard));

    let vague_sphere = Rc::new(Metal::new(Color::white(), 1.0));
    let smooth_sphere = Rc::new(Metal::new(Color::white(), 0.2));

    // Next, build a box of size (1, 1, 1), with in-looking window inside the XY plane
    let mut world = HittableList::default();

    // Back wall
    let bw_upper_left = Triangle::new(
        Vec3::new(-0.5, 0.0, 1.0),
        Vec3::new(0.5, 1.0, 1.0),
        Vec3::new(-0.5, 1.0, 1.0),
        back_wall.clone()
    );
    let bw_lower_right = Triangle::new(
        Vec3::new(-0.5, 0.0, 1.0),
        Vec3::new(0.5, 0.0, 1.0),
        Vec3::new(0.5, 1.0, 1.0),
        back_wall.clone()
    );

    // Front wall
    let fw_upper_left = Triangle::new(
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(0.5, 1.0, -1.0),
        Vec3::new(-0.5, 1.0, -1.0),
        blue_wall.clone()
    );
    let fw_lower_right = Triangle::new(
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(0.5, 0.0, -1.0),
        Vec3::new(0.5, 1.0, -1.0),
        blue_wall.clone()
    );

    // Left wall
    let lw_upper_front = Triangle::new(
        Vec3::new(-0.5, 1.0, -1.0),
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(-0.5, 1.0, 1.0),
        red_wall.clone()
    );
    let lw_lower_back = Triangle::new(
        Vec3::new(-0.5, 1.0, 1.0),
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(-0.5, 0.0, 1.0),
        red_wall.clone()
    );

    // Right wall
    let rw_upper_back = Triangle::new(
        Vec3::new(0.5, 0.0, -1.0),
        Vec3::new(0.5, 1.0, -1.0),
        Vec3::new(0.5, 0.0, 1.0),
        green_wall.clone()
    );
    let rw_lower_front = Triangle::new(
        Vec3::new(0.5, 0.0, 1.0),
        Vec3::new(0.5, 1.0, -1.0),
        Vec3::new(0.5, 1.0, 1.0),
        green_wall.clone()
    );

    // Floor
    let fl_back_left  = Triangle::new(
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(0.5, 0.0, 1.0),
        Vec3::new(-0.5, 0.0, 1.0),
        floor.clone()
    );
    let fl_front_right  = Triangle::new(
        Vec3::new(-0.5, 0.0, -1.0),
        Vec3::new(0.5, 0.0, -1.0),
        Vec3::new(0.5, 0.0, 1.0),
        floor.clone()
    );

    // Roof
    let ro_front_left  = Triangle::new(
        Vec3::new(-0.5, 1.0, 1.0),
        Vec3::new(0.5, 1.0, -1.0),
        Vec3::new(-0.5, 1.0, -1.0),
        white_wall.clone()
    );
    let ro_back_right  = Triangle::new(
        Vec3::new(-0.5, 1.0, 1.0),
        Vec3::new(0.5, 1.0, 1.0),
        Vec3::new(0.5, 1.0, -1.0),
        white_wall.clone()
    );

    // Roof Light
    let light_front_left = Triangle::new(
        Vec3::new(-0.25, 1.0, 0.75),
        Vec3::new(0.25, 1.0, 0.25),
        Vec3::new(-0.25, 1.0, 0.25),
        ceiling_light.clone()
    );
    let light_back_right = Triangle::new(
        Vec3::new(-0.25, 1.0, 0.75),
        Vec3::new(0.25, 1.0, 0.75),
        Vec3::new(0.25, 1.0, 0.25),
        ceiling_light.clone()
    );

    let left_front_sphere = Sphere::new(
        Point3{e: [-0.25, 0.2, 0.25]}, 0.2, vague_sphere.clone()
    );

    let right_front_sphere = Sphere::new(
        Point3{e: [0.25, 0.3, 0.5]}, 0.3, smooth_sphere.clone()
    );

    // Back Wall
    world.add(Box::new(bw_upper_left));
    world.add(Box::new(bw_lower_right));

    // Front Wall
    world.add(Box::new(fw_upper_left));
    world.add(Box::new(fw_lower_right));

    // Left Wall
    world.add(Box::new(lw_upper_front));
    world.add(Box::new(lw_lower_back));

    // Right Wall
    world.add(Box::new(rw_upper_back));
    world.add(Box::new(rw_lower_front));

    // Floor
    world.add(Box::new(fl_back_left));
    world.add(Box::new(fl_front_right));

    // Roof
    world.add(Box::new(ro_back_right));
    world.add(Box::new(ro_front_left));

    // Roof Light
    world.add(Box::new(light_front_left));
    world.add(Box::new(light_back_right));

    // Spheres
    world.add(Box::new(left_front_sphere));
    world.add(Box::new(right_front_sphere));

    camera.aspect_ratio = 1.0;
    camera.image_height = 512;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;
    camera.vfov = 90.0;
    camera.lookfrom = Point3{e: [0.0, 0.5, -0.5]};
    camera.lookat = Point3{e: [0.0, 0.5, 0.5]};
    camera.vup = Vec3::new(0.0, 1.0, 0.0);
    camera.defocus_angle = 0.0;
    camera.focus_dist = 1.5;
    camera.background_color = Color{e: [0.0, 0.0, 0.0]};

    return world;
}

fn simple_spheres(camera: &mut Camera) -> HittableList
{
    // World
    let checkerboard = Rc::new(CheckerTexture::new(
        Color::white(),
        Color::black(),
        4.0
    ));

    let material_ground = Rc::new(Lambertian::from_solid_color(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::from_texture(checkerboard));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_bubble = Rc::new(Dielectric::new(1.0 / 1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));
    let material_light = Rc::new(DiffuseLight::from_solid_color(Color{e: [1.0, 1.0, 1.0]}));

    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.4, material_bubble)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 1.0, -2.0]}, 0.2, material_light)));

    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 8;
    camera.vfov = 20.0;
    camera.lookfrom = Point3{e: [-2.0, 2.0, 1.0]};
    camera.lookat = Point3{e: [0.0, 0.0, -1.0]};
    camera.vup = Vec3::new(0.0, 1.0, 0.0);
    camera.defocus_angle = 5.0;
    camera.focus_dist = 3.4;
    camera.background_color = Color{e: [0.5, 0.5, 1.0]};

    return world;
}

fn simple_light(camera: &mut Camera) -> HittableList
{
    let mut hittable_list = HittableList::default();

    let checkerboard = Rc::new(CheckerTexture::new(
        Color::white(),
        Color::black(),
        4.0
    ));
    let red_blue_checkerboard = Rc::new(CheckerTexture::new(
        Color::from(8.0 * Vec3{e: [0.0, 0.0, 1.0]}),
        Color::from(8.0 * Vec3{e: [1.0, 0.0, 0.0]}),
        2.0
    ));
    let solid_white = Rc::new(SolidTexture::new(Color::white()));

    let ground = Sphere::new(Point3{e: [0.0, -1000.0, 0.0]}, 1000.0, Rc::new(Lambertian::from_texture(solid_white.clone())));
    let sphere = Sphere::new(Point3{e: [0.0, 2.0, 0.0]}, 2.0, Rc::new(Lambertian::from_texture(checkerboard.clone())));

    let light = Sphere::new(Point3{e: [3.0, 1.0, -1.0]}, 1.0, Rc::new(DiffuseLight::from_texture(red_blue_checkerboard)));

    hittable_list.add(Box::new(ground));
    hittable_list.add(Box::new(sphere));
    hittable_list.add(Box::new(light));

    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 64;
    camera.vfov = 20.0;
    camera.lookfrom = Point3{e: [26.0, 3.0, 6.0]};
    camera.lookat = Point3{e: [0.0, 2.0, 0.0]};
    camera.vup = Vec3::new(0.0, 1.0, 0.0);
    camera.defocus_angle = 0.0;
    camera.background_color = Color{e: [0.0, 0.0, 0.0]};

    return hittable_list;
}


fn main()
{
    let mut camera = Camera::default();

    // let world = simple_spheres(&mut camera);
    // let world = simple_light(&mut camera);
    let world = cornell_box(&mut camera);

     let world_box: Box<dyn Hittable> = Box::new(world);

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
