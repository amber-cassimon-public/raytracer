use std::fmt::{Display, Formatter};
use crate::{interval::Interval, vec3::Vec3};

pub struct Color
{
    pub e: [f32; 3],
}

impl Color
{
    pub fn white() -> Color
    {
        return Color{e: [1.0, 1.0, 1.0]};
    }

    pub fn black() -> Color
    {
        return Color{e: [0.0, 0.0, 0.0]};
    }

    pub fn red() -> Color
    {
        return Color{e: [1.0, 0.0, 0.0]};
    }

    pub fn green() -> Color
    {
        return Color{e: [0.0, 1.0, 0.0]};
    }

    pub fn blue() -> Color
    {
        return Color{e: [0.0, 0.0, 1.0]};
    }
}

impl From<Vec3> for Color
{
    fn from(value: Vec3) -> Self {
        return Color{e: value.e};
    }
}

impl Into<Vec3> for Color
{
    fn into(self) -> Vec3
    {
        return Vec3{e: self.e};
    }
}

pub fn linear_to_gamma(linear_component: f32) -> f32
{
    if 0.0 < linear_component
    {
        return linear_component.sqrt();
    }

    return 0.0;
}

impl Display for Color
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        const INTENSITY: Interval = Interval{min: 0.000, max: 0.999};

        let red = linear_to_gamma(self.e[0]);
        let green = linear_to_gamma(self.e[1]);
        let blue = linear_to_gamma(self.e[2]);

        let red_u8 = (256.0 * INTENSITY.clamp(red)) as u8;
        let green_u8 = (256.0 * INTENSITY.clamp(green)) as u8;
        let blue_u8 = (256.0 * INTENSITY.clamp(blue)) as u8;

        return write!(f, "{} {} {}\n", red_u8, green_u8, blue_u8);
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_display()
    {
        let purple: Color = Color{ e: [1.0, 0.0, 1.0]};
        let string = format!("{}", purple);

        assert_eq!("255 0 255\n", string);
    }

    #[test]
    fn test_from_vec3()
    {
        let purple_vec3: Vec3 = Vec3{e: [1.0, 0.0, 1.0]};
        let purple = Color::from(purple_vec3);

        assert_eq!([1.0, 0.0, 1.0], purple.e);
    }
}