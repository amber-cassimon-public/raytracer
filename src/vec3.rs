use std::fmt::{Display, Formatter};
use rand::{Rng, RngCore};

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Vec3
{
    pub e: [f32; 3]
}

impl Vec3
{
    pub fn zero() -> Vec3 {
        return Vec3::new(0.0, 0.0, 0.0);
    }

    pub fn new(e0: f32, e1: f32, e2: f32) -> Vec3 {
        return Vec3{e: [e0, e1, e2]};
    }

    pub fn random(min: f32, max: f32, rng: Option<&mut dyn RngCore>) -> Vec3
    {
        let mut thread_rng =  rand::thread_rng();
        let mut rng_ref: &mut dyn RngCore = rng.unwrap_or(&mut thread_rng);

        return Vec3{e: [
            min + ((max - min) * rng_ref.gen::<f32>()),
            min + ((max - min) * rng_ref.gen::<f32>()),
            min + ((max - min) * rng_ref.gen::<f32>())
        ]};
    }

    pub fn random_in_unit_sphere(rng: Option<&mut dyn RngCore>) -> Vec3
    {
        let mut thread_rng =  rand::thread_rng();
        let rng_ref: &mut dyn RngCore = rng.unwrap_or(&mut thread_rng);

        loop {
            let p = Vec3::random(-1.0, 1.0, Some(rng_ref));

            if p.length_squared() < 1.0
            {
                return p;
            }
        }
    }

    pub fn random_unit_vector(rng: Option<&mut dyn RngCore>) -> Vec3
    {
        return unit_vector(Self::random_in_unit_sphere(rng));
    }

    pub fn random_on_hemisphere(normal: &Vec3, rng: Option<&mut dyn RngCore>) -> Vec3
    {
        let on_unit_sphere = Vec3::random_unit_vector(rng);

        return if dot(on_unit_sphere, normal.clone()) < 0.0
        {
            -on_unit_sphere
        } else {
            on_unit_sphere
        }
    }

    pub fn random_in_unit_disk(rng: Option<&mut dyn RngCore>) -> Vec3
    {
        let mut thread_rng =  rand::thread_rng();
        let rng_ref: &mut dyn RngCore = rng.unwrap_or(&mut thread_rng);

        loop {
            let mut p = Vec3::random(-1.0, 1.0, Some(rng_ref));
            p.e[2] = 0.0;

            if p.length_squared() < 1.0
            {
                return p;
            }
        }
    }

    pub fn reflect(vector: &Vec3, normal: &Vec3) -> Vec3
    {
        return vector.clone() - (2.0 * dot(vector.clone(), normal.clone()) * normal.clone());
    }

    pub fn refract(vector: &Vec3, normal: &Vec3, eta_ratio: f32) -> Vec3
    {
        let cos_theta = f32::min(dot(-vector.clone(), normal.clone()),1.0);
        let r_out_perp = eta_ratio * (vector.clone() + (cos_theta * normal.clone()));
        let r_out_parallel = -(f32::abs(1.0 - r_out_perp.length_squared())).sqrt() * normal.clone();

        return r_out_perp + r_out_parallel;
    }

    pub fn x(&self) -> f32
    {
        return self.e[0];
    }

    pub fn y(&self) -> f32
    {
        return self.e[1];
    }

    pub fn z(&self) -> f32
    {
        return self.e[2];
    }

    pub fn length(&self) -> f32
    {
        return self.length_squared().sqrt();
    }

    pub fn length_squared(&self) -> f32
    {
        return (self.e[0] * self.e[0]) + (self.e[1] * self.e[1]) + (self.e[2] * self.e[2]);
    }

    pub fn near_zero(&self) -> bool
    {
        const S: f32 = 1e-8;
        return (self.e[0].abs() < S) && (self.e[1].abs() < S) && (self.e[2].abs() < S);
    }
}

pub fn dot(lhs: Vec3, rhs: Vec3) -> f32
{
    return (lhs.e[0] * rhs.e[0]) + (lhs.e[1] * rhs.e[1]) + (lhs.e[2] * rhs.e[2]);
}

pub fn cross(lhs: Vec3, rhs: Vec3) -> Vec3
{
    return Vec3{e:[
        (lhs.e[1] * rhs.e[2]) - (lhs.e[2] * rhs.e[1]),
        (lhs.e[2] * rhs.e[0]) - (lhs.e[0] * rhs.e[2]),
        (lhs.e[0] * rhs.e[1]) - (lhs.e[1] * rhs.e[0])
    ]};
}

pub fn unit_vector(vec: Vec3) -> Vec3
{
    let len = vec.length();
    return vec / len;
}

impl std::ops::Neg for Vec3
{
    type Output = Vec3;

    fn neg(self) -> Self::Output
    {
        return Vec3::new(-self.e[0], -self.e[1], -self.e[2]);
    }
}

impl std::ops::Index<usize> for Vec3
{
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output
    {
        if 3 <= index
        {
            panic!("index must be <3!");
        }

        return &self.e[index];
    }
}

impl std::ops::IndexMut<usize> for Vec3
{
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        if 3 <= index
        {
            panic!("index must be <3!");
        }

        return &mut self.e[index];
    }
}

impl std::ops::Add for Vec3
{
    type Output = Vec3;

    fn add(self, rhs: Self) -> Self::Output {
        return Vec3 {e: [
            self.e[0] + rhs.e[0],
            self.e[1] + rhs.e[1],
            self.e[2] + rhs.e[2]
        ]};
    }
}

impl std::ops::Sub for Vec3
{
    type Output = Vec3;

    fn sub(self, rhs: Self) -> Self::Output {
        return Vec3{e: [
            self.e[0] - rhs.e[0],
            self.e[1] - rhs.e[1],
            self.e[2] - rhs.e[2]
        ]};
    }
}

impl std::ops::AddAssign for Vec3
{
    fn add_assign(&mut self, rhs: Self) {
        self.e[0] += rhs.e[0];
        self.e[1] += rhs.e[1];
        self.e[2] += rhs.e[2];
    }
}

impl std::ops::Mul for Vec3
{
    type Output = Vec3;

    fn mul(self, rhs: Self) -> Self::Output {
        return Vec3 {e: [
            self.e[0] * rhs.e[0],
            self.e[1] * rhs.e[1],
            self.e[2] * rhs.e[2]
        ]};
    }
}

impl std::ops::Mul<f32> for Vec3
{
    type Output = Vec3;

    fn mul(self, rhs: f32) -> Self::Output {
        return Vec3 {e: [
            self.e[0] * rhs,
            self.e[1] * rhs,
            self.e[2] * rhs
        ]};
    }
}

impl std::ops::Mul<Vec3> for f32
{
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        return Vec3 {e: [
            self * rhs.e[0],
            self * rhs.e[1],
            self * rhs.e[2]
        ]};
    }
}

impl std::ops::MulAssign<f32> for Vec3
{
    fn mul_assign(&mut self, rhs: f32) {
        self.e[0] *= rhs;
        self.e[1] *= rhs;
        self.e[2] *= rhs;
    }
}

impl std::ops::Div<f32> for Vec3
{
    type Output = Vec3;

    fn div(self, rhs: f32) -> Self::Output {
        return Vec3 {e: [
            self.e[0] / rhs,
            self.e[1] / rhs,
            self.e[2] / rhs
        ]};
    }
}

impl std::ops::DivAssign<f32> for Vec3
{
    fn div_assign(&mut self, rhs: f32) {
        self.e[0] /= rhs;
        self.e[1] /= rhs;
        self.e[2] /= rhs;
    }
}

impl Display for Vec3
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.e[0], self.e[1], self.e[2])
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_cross()
    {
        let vector1 = Vec3::new(0.0, 1.0, 2.0);
        let vector2 = Vec3::new(2.0, 3.0, 4.0);

        let cross_product = cross(vector1, vector2);

        assert_eq!(-2.0, cross_product.e[0]);
        assert_eq!(4.0, cross_product.e[1]);
        assert_eq!(-2.0, cross_product.e[2]);

        let x = Vec3::new(1.0, 0.0, 0.0);
        let y = Vec3::new(0.0, 1.0, 0.0);

        let z = cross(x, y);

        assert_eq!(0.0, z.e[0]);
        assert_eq!(0.0, z.e[1]);
        assert_eq!(1.0, z.e[2]);

        // Test case from: https://www.mathsisfun.com/algebra/vectors-cross-product.html
        let a = Vec3::new(2.0, 3.0, 4.0);
        let b = Vec3::new(5.0,  6.0, 7.0);

        let a_x_b  = cross(a, b);

        assert_eq!([-3.0, 6.0, -3.0], a_x_b.e);
    }

    #[test]
    fn test_dot()
    {
        let vector1 = Vec3::new(0.0, 1.0, 2.0);
        let vector2 = Vec3::new(2.0, 3.0, 4.0);

        let dot_product = dot(vector1, vector2);

        assert_eq!(11.0, dot_product);
    }

    #[test]
    fn test_unit_vector()
    {
        let vector = Vec3::new(0.0, 2.0, 0.0);

        let normalized = unit_vector(vector);

        assert_eq!(0.0, normalized.e[0]);
        assert_eq!(1.0, normalized.e[1]);
        assert_eq!(0.0, normalized.e[2]);
    }

    #[test]
    fn test_zero()
    {
        let zero_vec = Vec3::zero();

        assert_eq!([0.0; 3], zero_vec.e);
        assert_eq!(0.0, zero_vec.x());
        assert_eq!(0.0, zero_vec.y());
        assert_eq!(0.0, zero_vec.z());
    }

    #[test]
    fn test_new()
    {
        let one_four_nine = Vec3::new(1.0, 4.0, 9.0);

        assert_eq!([1.0, 4.0, 9.0], one_four_nine.e);
        assert_eq!(1.0, one_four_nine.x());
        assert_eq!(4.0, one_four_nine.y());
        assert_eq!(9.0, one_four_nine.z());
    }

    #[test]
    fn test_negate()
    {
        let one_four_nine = Vec3::new(1.0, 4.0, 9.0);
        let negative = -one_four_nine;

        assert_eq!([-1.0, -4.0, -9.0], negative.e);
        assert_eq!(-1.0, negative.x());
        assert_eq!(-4.0, negative.y());
        assert_eq!(-9.0, negative.z());
    }

    #[test]
    fn test_index()
    {
        let one_four_nine = Vec3::new(1.0, 4.0, 9.0);

        assert_eq!(1.0, one_four_nine[0]);
        assert_eq!(4.0, one_four_nine[1]);
        assert_eq!(9.0, one_four_nine[2]);
    }

    #[test]
    #[should_panic]
    fn test_index_out_of_bounds()
    {
        let one_four_nine = Vec3::new(1.0, 4.0, 9.0);
        one_four_nine[3];
    }

    #[test]
    fn test_index_mut()
    {
        let mut one_four_nine = Vec3::new(1.0, 4.0, 9.0);
        one_four_nine[0] = 2.0;
        one_four_nine[1] = 16.0;
        one_four_nine[2] = 81.0;

        assert_eq!(2.0, one_four_nine.e[0]);
        assert_eq!(16.0, one_four_nine.e[1]);
        assert_eq!(81.0, one_four_nine.e[2]);
    }

    #[test]
    #[should_panic]
    fn test_index_mut_out_of_bounds()
    {
        let mut one_four_nine = Vec3::new(1.0, 4.0, 9.0);
        one_four_nine[3] = 5.0;
    }

    #[test]
    fn test_add()
    {
        let lhs = Vec3::new(0.0, 1.0, 2.0);
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let sum = lhs + rhs;

        assert_eq!(1.0, sum.e[0]);
        assert_eq!(5.0, sum.e[1]);
        assert_eq!(11.0, sum.e[2]);
    }

    #[test]
    fn test_add_assign()
    {
        let mut zero = Vec3::zero();
        let one_four_nine = Vec3::new(1.0, 4.0, 9.0);

        zero += one_four_nine;

        assert_eq!(1.0, zero.e[0]);
        assert_eq!(4.0, zero.e[1]);
        assert_eq!(9.0, zero.e[2]);
    }

    #[test]
    fn test_sub()
    {
        let lhs = Vec3::new(0.0, 1.0, 2.0);
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let sum = lhs - rhs;

        assert_eq!(-1.0, sum.e[0]);
        assert_eq!(-3.0, sum.e[1]);
        assert_eq!(-7.0, sum.e[2]);
    }

    #[test]
    fn test_mul()
    {
        let lhs = Vec3::new(0.0, 1.0, 2.0);
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let product = lhs * rhs;

        assert_eq!(0.0, product.e[0]);
        assert_eq!(4.0, product.e[1]);
        assert_eq!(18.0, product.e[2]);
    }

    #[test]
    fn test_scalar_vector_mul()
    {
        let lhs = 2.0f32;
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let product = rhs * lhs;

        assert_eq!(2.0, product.e[0]);
        assert_eq!(8.0, product.e[1]);
        assert_eq!(18.0, product.e[2]);
    }

    #[test]
    fn test_vector_scalar_mul()
    {
        let lhs = 2.0f32;
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let product = rhs * lhs;

        assert_eq!(2.0, product.e[0]);
        assert_eq!(8.0, product.e[1]);
        assert_eq!(18.0, product.e[2]);
    }

    #[test]
    fn test_mul_assign()
    {
        let mut zero_one_two = Vec3::new(0.0, 1.0, 2.0);

        zero_one_two *= 2.0;

        assert_eq!(0.0, zero_one_two.e[0]);
        assert_eq!(2.0, zero_one_two.e[1]);
        assert_eq!(4.0, zero_one_two.e[2]);
    }

    #[test]
    fn test_vector_scalar_div()
    {
        let lhs = 2.0f32;
        let rhs = Vec3::new(1.0, 4.0, 9.0);

        let product = rhs / lhs;

        assert_eq!(0.5, product.e[0]);
        assert_eq!(2.0, product.e[1]);
        assert_eq!(4.5, product.e[2]);
    }

    #[test]
    fn test_div_assign()
    {
        let mut zero_one_two = Vec3::new(0.0, 1.0, 2.0);

        zero_one_two /= 2.0;

        assert_eq!(0.0, zero_one_two.e[0]);
        assert_eq!(0.5, zero_one_two.e[1]);
        assert_eq!(1.0, zero_one_two.e[2]);
    }

    #[test]
    fn test_length_squared()
    {
        let vector = Vec3::new(1.0, 2.0, 3.0);

        assert_eq!(14.0, vector.length_squared());
    }

    #[test]
    fn test_length()
    {
        let vector = Vec3::new(1.0, 2.0, 3.0);

        assert_eq!(3.74165738677, vector.length());
    }

    #[test]
    fn test_display()
    {
        let vector = Vec3::new(1.0, 2.0, 3.0);

        #[allow(clippy::approx_constant)]
        let vector2 = Vec3::new(3.1415, 6.2830, 9.4245);

        assert_eq!("1 2 3", format!("{}", vector));
        assert_eq!("3.1415 6.283 9.4245", format!("{}", vector2));
    }

    #[test]
    fn test_near_zero()
    {
        let vector = Vec3::new(1.0, 2.0, 3.0);
        let near_zero = Vec3::new(5e-9, 1e-9, 1e-10);

        assert!(!vector.near_zero());
        assert!(near_zero.near_zero());
    }

    #[test]
    fn test_reflect()
    {
        // Test in a 2D plane
        let incoming = Vec3::new(1.0, -1.0, 0.0);
        let normal = Vec3::new(0.0, 1.0, 0.0);

        let reflected = Vec3::reflect(&incoming, &normal);

        assert_eq!([1.0, 1.0, 0.0], reflected.e);
    }

    #[test]
    fn test_refract()
    {
        // Test in a 2D plane
        let incoming = Vec3::new(1.0, -1.0, 0.0);
        let normal = Vec3::new(0.0, 1.0, 0.0);
        let eta_ratio = 2.0;

        let reflected = Vec3::refract(&incoming, &normal, eta_ratio);

        assert_eq!([2.0, -(3.0f32.sqrt()), 0.0], reflected.e);
    }
}