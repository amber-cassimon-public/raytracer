use crate::{point3::Point3, vec3::Vec3};

pub struct Ray
{
    orig: Point3,
    dir: Vec3
}

impl Ray
{
    pub fn new(origin: Point3, direction: Vec3) -> Ray
    {
        return Ray{
            orig: origin,
            dir: direction
        };
    }

    pub fn origin(&self) -> &Point3
    {
        return &self.orig;
    }

    pub fn direction(&self) -> &Vec3
    {
        return &self.dir;
    }

    pub fn at(&self, t: f32) -> Point3
    {
        let o_vec3: Vec3 = (*self.origin()).into();
        let d_vec3: Vec3 = self.dir.clone();
        let result = o_vec3 + (d_vec3 * t);

        return Point3::from(result);
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_at()
    {
        let x_ray = Ray::new(
            Point3{e: [0.0, 0.0, 0.0]},
            Vec3{e: [1.0, 0.0, 0.0]}
        );

        assert_eq!([0.0, 0.0, 0.0], x_ray.at(0.0).e);
        assert_eq!([0.5, 0.0, 0.0], x_ray.at(0.5).e);
        assert_eq!([1.0, 0.0, 0.0], x_ray.at(1.0).e);
        assert_eq!([2.0, 0.0, 0.0], x_ray.at(2.0).e);
    }
}