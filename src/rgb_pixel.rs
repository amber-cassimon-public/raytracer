#[derive(Copy, Clone, Debug, PartialEq)]
pub struct RgbPixel
{
    pub r: f32,
    pub g: f32,
    pub b: f32
}

impl RgbPixel
{
    pub fn white() -> RgbPixel
    {
        return RgbPixel{r: 1.0, g: 1.0, b: 1.0};
    }

    pub fn black() -> RgbPixel
    {
        return RgbPixel{r: 0.0, g: 0.0, b: 0.0};
    }

    pub fn red() -> RgbPixel
    {
        return RgbPixel{r: 1.0, g: 0.0, b: 0.0};
    }

    pub fn green() -> RgbPixel
    {
        return RgbPixel{r: 0.0, g: 1.0, b: 0.0};
    }

    pub fn blue() -> RgbPixel
    {
        return RgbPixel{r: 0.0, g: 0.0, b: 1.0};
    }

    pub fn apply(self, f: fn(f32) -> f32) -> RgbPixel
    {
        return RgbPixel{
            r: f(self.r),
            g: f(self.g),
            b: f(self.b)
        };
    }
}

impl std::ops::Add<RgbPixel> for RgbPixel
{
    type Output = RgbPixel;

    fn add(self, rhs: Self) -> Self::Output {
        return RgbPixel{
            r: self.r + rhs.r,
            g: self.g + rhs.g,
            b: self.b + rhs.b
        };
    }
}

impl std::ops::AddAssign<RgbPixel> for RgbPixel
{
    fn add_assign(&mut self, rhs: RgbPixel) {
        self.r += rhs.r;
        self.g += rhs.g;
        self.b += rhs.b;
    }
}

impl std::ops::Mul<f32> for RgbPixel
{
    type Output = RgbPixel;

    fn mul(self, rhs: f32) -> Self::Output {
        return RgbPixel{
            r: self.r * rhs,
            g: self.g * rhs,
            b: self.b * rhs
        };
    }
}

impl std::ops::Mul<RgbPixel> for f32
{
    type Output = RgbPixel;

    fn mul(self, rhs: RgbPixel) -> Self::Output {
        return rhs * self;
    }
}

impl std::ops::MulAssign<f32> for RgbPixel
{
    fn mul_assign(&mut self, rhs: f32) {
        self.r *= rhs;
        self.g *= rhs;
        self.b *= rhs;
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_zero_neural_element_under_addition()
    {
        let zeros = RgbPixel{r: 0.0, g: 0.0, b: 0.0};
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};

        assert_eq!(range, zeros + range);
    }

    #[test]
    fn test_add_one()
    {
        let ones = RgbPixel{r: 1.0, g: 1.0, b: 1.0};
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 1.1, g: 1.2, b: 1.3};

        assert_eq!(expected, ones + range);
    }

    #[test]
    fn test_add_range()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.2, g: 0.4, b: 0.6};

        assert_eq!(expected, range + range);
    }

    #[test]
    fn test_zero_neural_element_under_addition_assign()
    {
        let mut zeros = RgbPixel{r: 0.0, g: 0.0, b: 0.0};
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};

        zeros += range;

        assert_eq!(range, zeros);
    }

    #[test]
    fn test_add_assign_one()
    {
        let mut ones = RgbPixel{r: 1.0, g: 1.0, b: 1.0};
        ones += RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 1.1, g: 1.2, b: 1.3};

        assert_eq!(expected, ones);
    }

    #[test]
    fn test_add_assign_range()
    {
        let mut range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        range += range;
        let expected = RgbPixel{r: 0.2, g: 0.4, b: 0.6};

        assert_eq!(expected, range);
    }

    #[test]
    fn test_post_mul_zero_absorbing_element()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.0, g: 0.0, b: 0.0};

        assert_eq!(expected, range * 0.0);
    }

    #[test]
    fn test_post_mul_one_identity_element()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.1, g: 0.2, b: 0.3};

        assert_eq!(expected, range * 1.0);
    }

    #[test]
    fn test_post_mul_double()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.2, g: 0.4, b: 0.6};

        assert_eq!(expected, range * 2.0);
    }

    #[test]
    fn test_mul_assign_zero_absorbing_element()
    {
        let mut range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        range *= 0.0;
        let expected = RgbPixel{r: 0.0, g: 0.0, b: 0.0};

        assert_eq!(expected, range);
    }

    #[test]
    fn test_mul_assign_one_identity_element()
    {
        let mut range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        range *= 1.0;
        let expected = RgbPixel{r: 0.1, g: 0.2, b: 0.3};

        assert_eq!(expected, range);
    }

    #[test]
    fn test_mul_assign_double()
    {
        let mut range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        range *= 2.0;
        let expected = RgbPixel{r: 0.2, g: 0.4, b: 0.6};

        assert_eq!(expected, range);
    }

    #[test]
    fn test_pre_mul_zero_absorbing_element()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.0, g: 0.0, b: 0.0};

        assert_eq!(expected, 0.0 * range);
    }

    #[test]
    fn test_pre_mul_one_identity_element()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.1, g: 0.2, b: 0.3};

        assert_eq!(expected, 1.0 * range);
    }

    #[test]
    fn test_pre_mul_double()
    {
        let range = RgbPixel{r: 0.1, g: 0.2, b: 0.3};
        let expected = RgbPixel{r: 0.2, g: 0.4, b: 0.6};

        assert_eq!(expected, 2.0 * range);
    }

    #[test]
    fn test_white()
    {
        let color = RgbPixel::white();
        let expected = RgbPixel{r: 1.0, g: 1.0, b: 1.0};

        assert_eq!(expected, color);
    }

    #[test]
    fn test_black()
    {
        let color = RgbPixel::black();
        let expected = RgbPixel{r: 0.0, g: 0.0, b: 0.0};

        assert_eq!(expected, color);
    }

    #[test]
    fn test_nan_comparison()
    {
        let r_nan = RgbPixel{r: f32::NAN, g: 0.0, b: 0.0};
        let g_nan = RgbPixel{r: 0.0, g: f32::NAN, b: 0.0};
        let b_nan = RgbPixel{r: 0.0, g: 0.0, b: f32::NAN};

        assert_ne!(r_nan, r_nan);
        assert_ne!(g_nan, g_nan);
        assert_ne!(b_nan, b_nan);
    }
}