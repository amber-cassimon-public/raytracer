use std::cmp::max;
use std::io;
use std::io::Write;
use std::path::Path;

extern crate image;

use crate::{color::Color, interval::Interval, hittables::hittable::Hittable, hit_record::HitRecord,
            point3::Point3, ray::Ray, render_info::RenderInfo, vec3::cross, vec3::unit_vector,
            vec3::Vec3, util::degrees_to_radians};
use crate::{framebuffer::Framebuffer};
use crate::ray_generator::RayGenerator;
use crate::rgb_pixel::RgbPixel;

pub struct Camera
{
    pub aspect_ratio: f32,
    pub image_height: u32,
    pub samples_per_pixel: u32,
    pub max_depth: u32,
    pub vfov: f32,
    pub lookfrom: Point3,
    pub lookat: Point3,
    pub vup: Vec3,
    pub defocus_angle: f32,
    pub focus_dist: f32,
    pub background_color: Color,

    image_width: u32,
    center: Point3,
    pixel00_loc: Point3,
    pixel_delta_u: Vec3,
    pixel_delta_v: Vec3,
    pixel_samples_scale: f32,
    u: Vec3,
    v: Vec3,
    w: Vec3,
    defocus_disk_u: Vec3,
    defocus_disk_v: Vec3,
}

impl Camera
{
    pub fn render(&mut self, world: &Box<dyn Hittable>) -> RenderInfo
    {
        let start = std::time::Instant::now();
        let mut ray_count: u64 = 0;

        self.initialize();

        let mut framebuffer = Framebuffer::new(
            self.image_width as usize,
            self.image_height as usize
        );

        let mut ray_generator = RayGenerator::new(
            self.image_width as usize,
            self.image_height as usize,
            self.samples_per_pixel as usize,
            self.center.into(),
            self.defocus_angle,
            self.defocus_disk_u,
            self.defocus_disk_v,
            self.pixel00_loc.into(),
            self.pixel_delta_u,
            self.pixel_delta_v,
            None
        );

        println!("Generating Initial Rays...");
        let mut initial_rays: Vec<((usize, usize), Ray)> = Vec::with_capacity(ray_generator.size_hint().0);
        initial_rays.extend(ray_generator);

        let n_initial_rays: u64 = initial_rays.len() as u64;
        let ray_gen_time= start.elapsed();

        println!("Rendering...");
        for ((row, col), ray) in initial_rays
        {
            if 0 == col
            {
                let remaining_scanlines = (self.image_height as usize) - row;
                let percentage = 100.0 * ((row as f32) / (self.image_height as f32));

                print!("\rScanlines Remaining: {remaining_scanlines:4} ({percentage:5.2}%)");
                io::stdout().flush().unwrap();
            }

            let x = self.ray_color(&ray, world, self.max_depth, &mut ray_count);

            framebuffer[(row, col)] += RgbPixel{r: x.e[0], g: x.e[1], b: x.e[2]};
        }

        for row in 0..self.image_height
        {
            for col in 0..self.image_width
            {
                framebuffer[(row as usize, col as usize)] *= self.pixel_samples_scale;
            }
        }

        println!("\rDone!                             ");

        let byte_vector: Vec<u8> = framebuffer.export(
            false,
            |px| -> RgbPixel {
                return px.apply(|x| if 0.0 < x { x.sqrt() } else { 0.0 });
            }
        );

        image::save_buffer(
            &Path::new("image.png"),
            byte_vector.as_slice(),
            self.image_width,
            self.image_height,
            image::ExtendedColorType::Rgb8
        ).unwrap();

        let render_time = start.elapsed();

        return RenderInfo{
            image_width: self.image_width,
            image_height: self.image_height,
            samples_per_pixel: self.samples_per_pixel,
            initial_ray_count: n_initial_rays,
            total_ray_count: ray_count,
            ray_gen_time,
            render_time
        };
    }

    fn initialize(&mut self) -> ()
    {
        self.image_width = max(1u32, ((self.image_height as f32) * self.aspect_ratio) as u32);
        self.center = self.lookfrom;

        let theta = degrees_to_radians(self.vfov);
        let h = (theta / 2.0).tan();

        let viewport_height: f32 = 2.0 * h * self.focus_dist;
        // Don't use `aspect_ratio` due to rounding and max(1, width)
        let viewport_width = viewport_height * ((self.image_width as f32) / (self.image_height as f32));

        self.w = unit_vector(<Point3 as Into<Vec3>>::into(self.lookfrom) - <Point3 as Into<Vec3>>::into(self.lookat));
        self.u = unit_vector(cross(self.vup, self.w.clone()));
        self.v = cross(self.w.clone(), self.u.clone());

        let viewport_u = viewport_width * self.u.clone();
        let viewport_v = viewport_height * -self.v.clone();

        self.pixel_delta_u = viewport_u / (self.image_width as f32);
        self.pixel_delta_v = viewport_v / (self.image_height as f32);

        let viewport_upper_left: Vec3 = <Point3 as Into<Vec3>>::into(self.center)
            - (self.focus_dist * self.w)
            - (viewport_u / 2.0)
            - (viewport_v / 2.0);

        self.pixel00_loc = Point3::from(viewport_upper_left + 0.5 * (self.pixel_delta_u + self.pixel_delta_v));

        let defocus_radius  = self.focus_dist * degrees_to_radians(self.defocus_angle / 2.0).tan();
        self.defocus_disk_u = self.u * defocus_radius;
        self.defocus_disk_v = self.v * defocus_radius;

        self.pixel_samples_scale = 1.0 / (self.samples_per_pixel as f32);
    }

    fn ray_color(&self, ray: &Ray, world: &Box<dyn Hittable>, depth: u32, ray_count: &mut u64) -> Color
    {
        if 0 == depth
        {
            return Color{e: [0.0, 0.0, 0.0]};
        }

        *ray_count = ray_count.saturating_add(1);

        let t_interval =  Interval::new(0.001, f32::INFINITY);
        let mut hit_record = HitRecord::default();

        if !world.hit(ray, &t_interval, &mut hit_record)
        {
            return Color{e: self.background_color.e};
        }

        let mut scattered = Ray::new(Point3::origin(), Vec3::zero());
        let mut attenuation = Color{e: [1.0, 1.0, 1.0]};
        let emission = hit_record.material.emitted(hit_record.u, hit_record.v, &hit_record.p);

        if !hit_record.material.scatter(ray, &hit_record, &mut attenuation, &mut scattered)
        {
            return emission;
        }

        let scatter: Vec3 = <Color as Into<Vec3>>::into(attenuation) * <Color as Into<Vec3>>::into(self.ray_color(&scattered, world, depth - 1, ray_count));

        return (<Color as Into<Vec3>>::into(emission) + scatter).into();
    }
}

impl Default for Camera
{
    fn default() -> Self {
        return Camera{
            aspect_ratio: 1.0,
            image_height: 32,
            samples_per_pixel: 1,
            max_depth: 1,
            vfov: 90.0,
            lookfrom: Point3{e: [0.0, 0.0, 0.0]},
            lookat: Point3{e: [0.0, 0.0, -1.0]},
            vup: Vec3{e: [0.0, 1.0, 0.0]},
            background_color: Color{e: [0.2, 0.3, 0.3]},
            image_width: 32,
            center: Point3::origin(),
            pixel00_loc: Point3::origin(),
            pixel_delta_u: Vec3::zero(),
            pixel_delta_v: Vec3::zero(),
            pixel_samples_scale: 1.0,
            u: Vec3::zero(),
            v: Vec3::zero(),
            w: Vec3::zero(),
            defocus_angle: 0.0,
            focus_dist: 1.0,
            defocus_disk_u: Vec3::zero(),
            defocus_disk_v: Vec3::zero(),
        };
    }
}