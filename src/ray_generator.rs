use rand::{Rng, RngCore, rngs::OsRng, rngs::StdRng, SeedableRng};
use crate::{ray::Ray, vec3::Vec3};

pub struct RayGenerator
{
    // Image
    pub image_width: usize,
    pub image_height: usize,
    pub samples_per_pixel: usize,

    // Camera
    camera_center: Vec3,
    defocus_angle: f32,
    defocus_u: Vec3,
    defocus_v: Vec3,

    // Image plane
    pixel_origin: Vec3,
    pixel_delta_u: Vec3,
    pixel_delta_v: Vec3,

    // State
    current_sample_index: usize,
    rng: Box<dyn RngCore>,
}

impl RayGenerator
{
    fn sample_square(&mut self) -> Vec3
    {
        return Vec3 {e: [
            self.rng.gen::<f32>() - 0.5,
            self.rng.gen::<f32>() - 0.5,
            0.0
        ]};
    }

    fn sample_defocus_disk(&mut self) -> Vec3
    {
        let unit_disk_point = Vec3::random_in_unit_disk(Some(&mut self.rng));
        return self.camera_center +
            (unit_disk_point.e[0] * self.defocus_u) +
            (unit_disk_point.e[1] * self.defocus_v);
    }

    fn decompose_sample_index(&self, sample_index: usize) -> (usize, usize, usize)
    {
        let pixel_sample = sample_index % self.samples_per_pixel;
        let col = ((sample_index - pixel_sample) / self.samples_per_pixel) % self.image_width;
        let row = (sample_index - pixel_sample - (self.samples_per_pixel * col)) / (self.samples_per_pixel  * self.image_width);

        return (row, col, pixel_sample);
    }

    fn done(&self) -> bool
    {
        return (self.samples_per_pixel * self.image_width * self.image_height) <= self.current_sample_index;
    }

    pub fn new(
        image_width: usize,
        image_height: usize,
        samples_per_pixel: usize,
        camera_center: Vec3,
        defocus_angle: f32,
        defocus_u: Vec3,
        defocus_v: Vec3,
        pixel_origin: Vec3,
        pixel_delta_u: Vec3,
        pixel_delta_v: Vec3,
        rng: Option<Box<dyn RngCore>>,
    ) -> RayGenerator
    {
        return RayGenerator {
            image_width,
            image_height,
            samples_per_pixel,
            camera_center,
            defocus_angle,
            defocus_u,
            defocus_v,
            pixel_origin,
            pixel_delta_u,
            pixel_delta_v,
            current_sample_index: 0,
            rng: rng.unwrap_or(Box::new(StdRng::from_rng(OsRng::default()).unwrap())),
        };
    }
}

impl Iterator for RayGenerator
{
    type Item = ((usize, usize), Ray);



    fn next(&mut self) -> Option<Self::Item>
    {
        if self.done()
        {
            return None;
        }

        let offset = self.sample_square();
        let (row, col, _): (usize, usize, usize) = self.decompose_sample_index(self.current_sample_index);

        let u: Vec3 = self.pixel_delta_u * ((col as f32) + offset.x());
        let v: Vec3 = self.pixel_delta_v * ((row as f32) + offset.y());

        let pixel_sample = self.pixel_origin + u + v;

        let ray_origin =  if self.defocus_angle <= 0.0 {
            self.camera_center
        } else {
            self.sample_defocus_disk()
        };
        let ray_direction = pixel_sample - ray_origin;
        let ray= Ray::new(ray_origin.into(), ray_direction);

        self.current_sample_index += 1;

        return Some(((row, col), ray));
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.image_width * self.image_height * self.samples_per_pixel;
        return (size, Some(size));
    }


    fn count(self) -> usize
    where
        Self: Sized,
    {
        return self.size_hint().0;
    }
}

#[cfg(test)]
mod tests
{
    use crate::point3::Point3;
    use super::*;

    #[test]
    fn test_count()
    {
        let gen = RayGenerator::new(
            568, 320,
            1024,
            Vec3 {e: [ 0.0, 0.0, -1.0] },
            0.5,
            Vec3{ e: [-1.0, 0.0, 0.0]},
            Vec3{ e: [0.0, -1.0, 0.0]},
            Vec3{ e: [-0.5, 0.5, 0.0]},
            Vec3 { e: [1.0, 0.0, 0.0] },
            Vec3 { e: [0.0, 1.0, 0.0] },
            Some(Box::new(StdRng::from_seed([0; 32]))),
        );

        assert_eq!(568 * 320 * 1024, gen.count());
    }

    #[test]
    fn test_index_decomposition()
    {
        let gen = RayGenerator::new(
            3, 2,
            16,
            Vec3 {e: [ 0.0, 0.0, -1.0] },
            0.5,
            Vec3{ e: [-1.0, 0.0, 0.0]},
            Vec3{ e: [0.0, -1.0, 0.0]},
            Vec3{ e: [-0.5, 0.5, 0.0]},
            Vec3 { e: [1.0, 0.0, 0.0] },
            Vec3 { e: [0.0, 1.0, 0.0] },
            Some(Box::new(StdRng::from_seed([0; 32]))),
        );

        // First Row, First Pixel
        assert_eq!((0, 0, 0), gen.decompose_sample_index(0));
        assert_eq!((0, 0, 1), gen.decompose_sample_index(1));
        assert_eq!((0, 0, 2), gen.decompose_sample_index(2));
        assert_eq!((0, 0, 15), gen.decompose_sample_index(15));

        // First Row, Second Pixel
        assert_eq!((0, 1, 0), gen.decompose_sample_index(16));
        assert_eq!((0, 1, 1), gen.decompose_sample_index(17));

        // First Row, Third Pixel
        assert_eq!((0, 1, 15), gen.decompose_sample_index(31));
        assert_eq!((0, 2, 0), gen.decompose_sample_index(32));

        // Second Row, First Pixel
        assert_eq!((0, 2, 15), gen.decompose_sample_index(47));
        assert_eq!((1, 0, 0), gen.decompose_sample_index(48));
    }

    #[test]
    fn test_done()
    {
        let mut gen = RayGenerator::new(
            3, 2,
            2,
            Vec3 {e: [ 0.0, 0.0, -1.0] },
            0.5,
            Vec3{ e: [-1.0, 0.0, 0.0]},
            Vec3{ e: [0.0, -1.0, 0.0]},
            Vec3{ e: [-0.5, 0.5, 0.0]},
            Vec3 { e: [1.0, 0.0, 0.0] },
            Vec3 { e: [0.0, 1.0, 0.0] },
            Some(Box::new(StdRng::from_seed([0; 32]))),
        );

        for _i in 0..(3 * 2 * 2)
        {
            assert!(!gen.done());
            gen.next();
        }

        assert!(gen.done());
    }

    #[test]
    fn test_ray_generation()
    {
        let mut gen = RayGenerator::new(
            3, 2,
            2,
            Vec3 {e: [ 0.0, 0.0, -1.0] },
            0.5,
            Vec3{ e: [-1.0, 0.0, 0.0]},
            Vec3{ e: [0.0, -1.0, 0.0]},
            Vec3{ e: [-0.5, 0.5, 0.0]},
            Vec3 { e: [1.0, 0.0, 0.0] },
            Vec3 { e: [0.0, 1.0, 0.0] },
            Some(Box::new(StdRng::from_seed([0; 32]))),
        );

        let ((row_0, col_0), ray_0) = gen.next().unwrap();
        assert_eq!(0, row_0);
        assert_eq!(0, col_0);
        assert_eq!([0.84018326, -0.2932434, -1.0], ray_0.origin().e);
        assert_eq!([-1.4237564, 0.62126666, 1.0], ray_0.direction().e);

        let ((row_1, col_1), ray_1) = gen.next().unwrap();
        assert_eq!(0, row_0);
        assert_eq!(0, col_0);
        assert_eq!([0.59250164, 0.050385237, -1.0], ray_1.origin().e);
        assert_eq!([-1.1160553, 0.7030858, 1.0], ray_1.direction().e);

        for _ in 2..(3 * 2 * 2) - 1
        {
            gen.next();
        }

        let ((row_n, col_n), ray_n) = gen.next().unwrap();
        assert_eq!(1, row_n);
        assert_eq!(2, col_n);
        assert_eq!([-0.60048914, 0.3094554, -1.0], ray_n.origin().e);
        assert_eq!([1.7986146, 0.88077426, 1.0], ray_n.direction().e);

        let none_ray = gen.next();
        assert!(none_ray.is_none());
    }

    #[test]
    fn test_square_sampling()
    {
        let mut gen = RayGenerator::new(
            568, 320,
            1024,
            Vec3 {e: [ 0.0, 0.0, -1.0] },
            0.5,
            Vec3{ e: [-1.0, 0.0, 0.0]},
            Vec3{ e: [0.0, -1.0, 0.0]},
            Vec3{ e: [-0.5, 0.5, 0.0]},
            Vec3 { e: [1.0, 0.0, 0.0] },
            Vec3 { e: [0.0, 1.0, 0.0] },
            Some(Box::new(StdRng::from_seed([0; 32]))),
        );

        let square_sample_1 = gen.sample_square();
        let square_sample_2 = gen.sample_square();
        let square_sample_3 = gen.sample_square();

        assert_eq!([-0.0835731, -0.17197675, 0.0], square_sample_1.e);
        assert_eq!([-0.42654234, 0.33403242, 0.0], square_sample_2.e);
        assert_eq!([0.23344666, -0.42009163, 0.0], square_sample_3.e);
    }
}