use std::vec::Vec;
use crate::rgb_pixel::RgbPixel;

pub struct Framebuffer
{
    pub width: usize,
    pub height: usize,
    pixels: Vec<RgbPixel>
}

impl Framebuffer
{
    pub fn new(width: usize, height: usize) -> Framebuffer
    {
        let _width = std::cmp::max(1, width);
        let _height = std::cmp::max(1, height);

        return Framebuffer{
            width: _width,
            height: _height,
            pixels: vec![RgbPixel::black(); _width * _height]
        }
    }

    pub fn size(&self) -> (usize, usize)
    {
        return (self.width, self.height);
    }

    pub fn export(&self, flip_horizontal: bool, gamma_transform: fn(RgbPixel) -> RgbPixel) -> Vec<u8>
    {
        let mut vector = vec![0; self.width * self.height * 3];

        for row in 0..self.height
        {
            for col in 0..self.width
            {
                let flipped_col = if flip_horizontal {self.width - col - 1} else {col};

                let gamma_space = gamma_transform(self[(row, flipped_col)]);
                let base_index = 3 * ((row * self.width) + flipped_col);

                vector[base_index]      = std::cmp::min(255, (256.0 * gamma_space.r) as u8);
                vector[base_index + 1]  = std::cmp::min(255, (256.0 * gamma_space.g) as u8);
                vector[base_index + 2]  = std::cmp::min(255, (256.0 * gamma_space.b) as u8);
            }
        }

        return vector;
    }
}

impl std::ops::Index<(usize, usize)> for Framebuffer
{
    type Output = RgbPixel;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        let row = index.0 % self.height;
        let col = index.1 % self.width;

        let linearized_index = (row * self.width) + col;

        return &(self.pixels[linearized_index]);
    }
}

impl std::ops::IndexMut<(usize, usize)> for Framebuffer
{
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        let row = index.0 % self.height;
        let col = index.1 % self.width;

        let linearized_index = (row * self.width) + col;

        return &mut (self.pixels[linearized_index]);
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_new_3x2()
    {
        let fb = Framebuffer::new(3, 2);

        assert_eq!(3, fb.width);
        assert_eq!(2, fb.height);
        assert_eq!((3, 2), fb.size());

        for row in 0..2usize
        {
            for col in 0..3usize
            {
                assert_eq!(RgbPixel::black(), fb[(row, col)]);
            }
        }

        assert_eq!(RgbPixel::black(), fb[(100, 100)]);
    }

    #[test]
    fn test_new_0x0()
    {
        let fb = Framebuffer::new(0, 0);

        assert_eq!(1, fb.width);
        assert_eq!(1, fb.height);
        assert_eq!((1, 1), fb.size());

        assert_eq!(RgbPixel::black(), fb[(0, 0)]);
        assert_eq!(RgbPixel::black(), fb[(100, 100)]);
    }

    #[test]
    fn test_create_and_read_ppm_example_wikipedia()
    {
        let mut fb = Framebuffer::new(3, 2);

        fb[(0, 0)] = RgbPixel::red();
        fb[(0, 1)] = RgbPixel::green();
        fb[(0, 2)] = RgbPixel::blue();
        fb[(1, 0)] = RgbPixel{r: 1.0, g: 1.0, b: 0.0};
        fb[(1, 1)] = RgbPixel::white();
        fb[(1, 2)] = RgbPixel::black();

        // First row
        assert_eq!(RgbPixel::red(),     fb[(0, 0)]);
        assert_eq!(RgbPixel::green(),   fb[(0, 1)]);
        assert_eq!(RgbPixel::blue(),    fb[(0, 2)]);

        // Check roll-over of indices
        assert_eq!(RgbPixel::red(),     fb[(0, 3)]);
        assert_eq!(RgbPixel::green(),   fb[(0, 4)]);
        assert_eq!(RgbPixel::blue(),    fb[(0, 5)]);

        // Second row
        assert_eq!(RgbPixel{r: 1.0, g: 1.0, b: 0.0},    fb[(1, 0)]);
        assert_eq!(RgbPixel::white(),   fb[(1, 1)]);
        assert_eq!(RgbPixel::black(),   fb[(1, 2)]);

        // Check roll-over of indices
        assert_eq!(RgbPixel{r: 1.0, g: 1.0, b: 0.0},     fb[(1, 3)]);
        assert_eq!(RgbPixel::white(),   fb[(1, 4)]);
        assert_eq!(RgbPixel::black(),   fb[(1, 5)]);

        // Double roll-over
        assert_eq!(RgbPixel::red(),     fb[(2, 3)]);
        assert_eq!(RgbPixel::green(),   fb[(2, 4)]);
        assert_eq!(RgbPixel::blue(),    fb[(2, 5)]);
    }

    #[test]
    fn test_ppm_example_wikipedia_to_vec_of_u8()
    {
        let mut fb = Framebuffer::new(3, 2);

        fb[(0, 0)] = RgbPixel::red();
        fb[(0, 1)] = RgbPixel::green();
        fb[(0, 2)] = RgbPixel::blue();
        fb[(1, 0)] = RgbPixel{r: 1.0, g: 1.0, b: 0.0};
        fb[(1, 1)] = RgbPixel::white();
        fb[(1, 2)] = RgbPixel::black();

        let vec: Vec<u8> = fb.export(false, |px| { return px; });

        assert_eq!(18, vec.len());

        // Top Left
        assert_eq!(255, vec[0]);
        assert_eq!(0,   vec[1]);
        assert_eq!(0,   vec[2]);

        // Top Middle
        assert_eq!(0,   vec[3]);
        assert_eq!(255, vec[4]);
        assert_eq!(0,   vec[5]);

        // Top Right
        assert_eq!(0,   vec[6]);
        assert_eq!(0,   vec[7]);
        assert_eq!(255, vec[8]);

        // Bottom Left
        assert_eq!(255, vec[9]);
        assert_eq!(255, vec[10]);
        assert_eq!(0,   vec[11]);

        // Bottom Middle
        assert_eq!(255, vec[12]);
        assert_eq!(255, vec[13]);
        assert_eq!(255, vec[14]);

        // Bottom Right
        assert_eq!(0,   vec[15]);
        assert_eq!(0,   vec[16]);
        assert_eq!(0,   vec[17]);
    }
}