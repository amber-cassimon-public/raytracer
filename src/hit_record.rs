use std::rc::Rc;
use crate::{color::Color, point3::Point3, ray::Ray,
            vec3::dot, vec3::Vec3};
use crate::materials::lambertian::Lambertian;
use crate::materials::material::Material;

#[derive(Clone)]
pub struct HitRecord
{
    pub p: Point3,
    pub normal: Vec3,
    pub material: Rc<dyn Material>,
    pub t: f32,
    pub u: f32,
    pub v: f32,
    pub front_face: bool
}

impl HitRecord
{
    pub fn set_face_normal(&mut self, ray: &Ray, outward_normal: &Vec3) -> ()
    {
        self.front_face = dot(ray.direction().clone(), outward_normal.clone()) < 0.0;
        self.normal = if self.front_face {outward_normal.clone()} else {-(outward_normal.clone())};
    }
}

impl Default for HitRecord
{
    fn default() -> Self {
        return HitRecord{
            p: Point3::origin(),
            normal: Vec3::zero(),
            material: Rc::new(Lambertian::from_solid_color(Color{e: [0.0, 0.0, 0.0]})),
            t: -1.0,
            u: -1.0,
            v: -1.0,
            front_face: false
        };
    }
}