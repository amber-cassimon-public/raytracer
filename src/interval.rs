pub  struct Interval
{
    pub min: f32,
    pub max: f32,
}

impl Interval
{
    const EMPTY: Interval = Interval{min: f32::INFINITY, max: -f32::INFINITY};
    const UNIVERSE: Interval = Interval{min: -f32::INFINITY, max: f32::INFINITY};

    pub fn new(min: f32, max: f32) -> Interval
    {
        return Interval{min, max };
    }

    pub fn size (&self) -> f32
    {
        return self.max - self.min;
    }

    pub fn contains(&self, x: f32) -> bool
    {
        return (self.min <= x) && (x <= self.max);
    }

    pub fn surrounds(&self, x: f32) -> bool
    {
        return (self.min < x) && (x < self.max);
    }

    pub fn clamp(&self, x: f32) -> f32
    {
        return if x < self.min {
            self.min
        } else if self.max < x {
            self.max
        } else {
            x
        };
    }
}

impl Default for Interval
{
    fn default() -> Self {
        // Default is an empty interval
        return Interval{min: f32::INFINITY, max: -f32::INFINITY};
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_size()
    {
        assert_eq!(1.0, Interval::new(0.0, 1.0).size());
        assert_eq!(1.0, Interval::new(100.0, 101.0).size());
        assert_eq!(100.0, Interval::new(0.0, 100.0).size());
    }

    #[test]
    fn test_contains()
    {
        assert!(Interval::new(0.0, 1.0).contains(0.5));
        assert!(Interval::new(0.0, 1.0).contains(0.0));
        assert!(Interval::new(0.0, 1.0).contains(1.0));
    }

    #[test]
    fn test_surrounds()
    {
        assert!(Interval::new(0.0, 1.0).contains(0.5));
        assert!(!Interval::new(0.0, 1.0).surrounds(0.0));
        assert!(!Interval::new(0.0, 1.0).surrounds(1.0));
    }

    #[test]
    fn test_clamp()
    {
        assert_eq!(0.0, Interval::new(0.0, 1.0).clamp(-1.0));
        assert_eq!(0.0, Interval::new(0.0, 1.0).clamp(0.0));
        assert_eq!(0.5, Interval::new(0.0, 1.0).clamp(0.5));
        assert_eq!(1.0, Interval::new(0.0, 1.0).clamp(1.0));
        assert_eq!(1.0, Interval::new(0.0, 1.0).clamp(2.0));
    }
}