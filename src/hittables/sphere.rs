use std::rc::Rc;
use crate::{hit_record::HitRecord, interval::Interval, point3::Point3, ray::Ray,
            vec3::dot, vec3::Vec3};
use crate::hittables::hittable::Hittable;
use crate::materials::material::Material;

pub struct Sphere
{
    center: Point3,
    radius: f32,
    material: Rc<dyn Material>
}

impl Sphere
{
    pub fn new(center: Point3, radius: f32, material: Rc<dyn Material>) -> Sphere
    {
        return Sphere{center, radius, material};
    }

    pub fn sphere_uv(p: &Point3, u: &mut f32, v: &mut f32) -> ()
    {
        let vec = <Point3 as Into<Vec3>>::into(p.clone());

        let theta = f32::acos(-vec.y());
        let phi = f32::atan2(-vec.z(), vec.x()) + std::f32::consts::PI;

        *u = phi / (2.0 * std::f32::consts::PI);
        *v = theta / std::f32::consts::PI;
    }
}

impl Hittable for Sphere
{
    fn hit(&self, ray: &Ray, ray_t: &Interval, hit_record: &mut HitRecord) -> bool
    {
        let oc = <Point3 as Into<Vec3>>::into(self.center) - <Point3 as Into<Vec3>>::into(ray.origin().clone());

        let a = ray.direction().length_squared();
        let h  = dot(ray.direction().clone(), oc);
        let c = oc.length_squared() - (self.radius * self.radius);

        let discriminant = (h * h) - (a * c);

        if discriminant < 0.0
        {
            return false;
        }

        let sqrtd = discriminant.sqrt();

        let mut root = (h - sqrtd) / a;

        if !ray_t.surrounds(root)
        {
            root = (h + sqrtd) / a;

            if !ray_t.surrounds(root)
            {
                return false;
            }
        }

        hit_record.t = root;
        hit_record.p = ray.at(root);

        let outward_normal = (<Point3 as Into<Vec3>>::into(hit_record.p) - <Point3 as Into<Vec3>>::into(self.center)) / self.radius;
        let uv_point= Point3::from(outward_normal);

        hit_record.set_face_normal(ray, &outward_normal);
        hit_record.material = self.material.clone();
        Sphere::sphere_uv(&uv_point, &mut hit_record.u, &mut hit_record.v);

        return true;
    }
}

#[cfg(test)]
mod tests
{
    use crate::color::Color;
    use crate::materials::lambertian::Lambertian;
    use super::*;

    #[test]
    fn test_hit()
    {
        let centered_ray = Ray::new(Point3{e: [0.0, 0.0, 1.0]}, Vec3{e: [0.0, 0.0, -1.0]});
        let offset_ray = Ray::new(Point3{e: [-0.5, 0.5, 1.0]}, Vec3{e: [0.0, 0.0, -1.0]});

        let sphere = Sphere::new(Point3::origin(), 0.5, Rc::new(Lambertian::from_solid_color(Color{e: [0.1, 0.5, 0.9]})));
        let t = Interval::new(-0.0, 1.0);

        let mut hit = HitRecord::default();
        let mut miss = HitRecord::default();

        assert!(sphere.hit(&centered_ray, &t, &mut hit));
        assert!(!sphere.hit(&offset_ray, &t, &mut miss));

        assert_eq!(Point3{e: [0.0, 0.0, 0.5]}, hit.p);
        assert_eq!(Vec3{e: [0.0, 0.0, 1.0]}, hit.normal);
        assert_eq!(0.5, hit.t);
        assert!(hit.front_face);

        assert_eq!(Point3{e: [0.0, 0.0, 0.0]}, miss.p);
        assert_eq!(Vec3{e: [0.0, 0.0, 0.0]}, miss.normal);
        assert_eq!(-1.0, miss.t);
        assert!(!miss.front_face);
    }

    #[test]
    fn test_uv_mapping()
    {
        let x_one = Point3{e: [1.0, 0.0, 0.0]};
        let y_one = Point3{e: [0.0, 1.0, 0.0]};
        let z_one = Point3{e: [0.0, 0.0, 1.0]};
        let x_minus_one = Point3{e: [-1.0, 0.0, 0.0]};
        let y_minus_one = Point3{e: [0.0, -1.0, 0.0]};
        let z_minus_one = Point3{e: [0.0, 0.0, -1.0]};

        let mut u = -1.0;
        let mut v = -1.0;

        Sphere::sphere_uv(&x_one, &mut u, &mut v);
        assert!(f32::abs(0.5 - u) < 0.00000004);
        assert!(f32::abs(0.5 - v) < 0.00000004);

        Sphere::sphere_uv(&y_one, &mut u, &mut v);
        assert!(f32::abs(0.5 - u) < 0.00000004);
        assert!(f32::abs(1.0 - v) < 0.00000006);

        Sphere::sphere_uv(&z_one, &mut u, &mut v);
        assert_eq!(0.25, u);
        assert!(f32::abs(0.5 - v) < 0.00000004);

        Sphere::sphere_uv(&x_minus_one, &mut u, &mut v);
        assert_eq!(0.0, u);
        assert!(f32::abs(0.5 - v) < 0.00000004);

        Sphere::sphere_uv(&y_minus_one, &mut u, &mut v);
        assert!(f32::abs(0.5 - u) < 0.00000004);
        assert_eq!(0.0, v);

        Sphere::sphere_uv(&z_minus_one, &mut u, &mut v);
        assert_eq!(0.75, u);
        assert!(f32::abs(0.5 - v) < 0.00000004);
    }
}