use crate::{interval::Interval, hit_record::HitRecord, ray::Ray};

pub trait Hittable
{
    fn hit (&self, ray: &Ray, ray_t: &Interval, hit_record: &mut HitRecord) -> bool;
}