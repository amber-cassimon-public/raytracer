use std::rc::Rc;
use crate::hit_record::HitRecord;
use crate::hittables::hittable::Hittable;
use crate::interval::Interval;
use crate::{point3::Point3, ray::Ray};
use crate::materials::material::Material;
use crate::vec3::{cross, dot, unit_vector, Vec3};

pub struct Triangle
{
    v0: Vec3,
    v1: Vec3,
    v2: Vec3,
    e01: Vec3,
    e02: Vec3,
    n: Vec3,
    d: f32,
    material: Rc<dyn Material>
}

impl Triangle
{
    pub fn new(v0: Vec3, v1: Vec3, v2: Vec3, material: Rc<dyn Material>) -> Triangle
    {
        let e01 = v1 - v0;
        let e02 = v2 - v0;
        let e01_x_e02 = cross(e01, e02);
        let n = -unit_vector(e01_x_e02);    // Counterclock winding order requires inversion of normal
        let d = dot(n, v0);

        return Triangle{v0, v1, v2, e01,  e02, n, d, material};
    }
}

impl Hittable for Triangle
{
    fn hit(&self, ray: &Ray, ray_t: &Interval, hit_record: &mut HitRecord) -> bool
    {
        let ray_x_e02 = cross(ray.direction().clone(), self.e02);
        let determinant = dot(self.e01, ray_x_e02);

        if determinant.abs() < f32::EPSILON
        {
            return false;
        }

        let inv_determinant = 1.0 / determinant;
        let s = <Point3 as Into<Vec3>>::into(ray.origin().clone()) - self.v0;
        let u  = inv_determinant * dot(s,  ray_x_e02);

        if (u < 0.0) || (1.0 < u)
        {
            return false;
        }

        let s_x_e01 = cross(s, self.e01);
        let v = inv_determinant * dot(ray.direction().clone(), s_x_e01);

        if (v < 0.0) || (1.0 < (u + v))
        {
            return false;
        }

        let t= inv_determinant * dot(self.e02, s_x_e01);

        if !ray_t.contains(t)
        {
            return false;
        }

        let intersection = ray.at(t);

        let u = intersection.e[0] + f32::abs(f32::floor(intersection.e[0]));
        let v = intersection.e[2] + f32::abs(f32::floor(intersection.e[2]));

        hit_record.t = t;
        hit_record.p = intersection;
        hit_record.material = self.material.clone();
        hit_record.set_face_normal(ray, &self.n);
        hit_record.u = u;
        hit_record.v = v;

        return true;
    }
}

#[cfg(test)]
mod tests
{
    use crate::color::Color;
    use crate::materials::lambertian::Lambertian;
    use super::*;

    #[test]
    fn test_intersection()
    {
        let material = Lambertian::from_solid_color(Color::black());

        let triangle = Triangle::new(
            Vec3{e: [-0.5, -0.5, 0.0]},
            Vec3{e: [ 0.5, -0.5, 0.0]},
            Vec3{e: [-0.5,  0.5, 0.0]},
            Rc::new(material)
        );
        let t = Interval::new(-0.0, 1.0);

        let centered_ray = Ray::new(Point3{e: [-0.25, -0.25, -0.5]}, Vec3{e:[0.0, 0.0, 1.0]});
        let offset_ray = Ray::new(Point3{e: [0.5, 0.5, -0.5]}, Vec3{e:[0.0, 0.0, 1.0]});

        let mut hit = HitRecord::default();
        let mut miss = HitRecord::default();

        assert!(triangle.hit(&centered_ray, &t, &mut hit));
        assert!(!triangle.hit(&offset_ray, &t, &mut miss));

        assert_eq!(Point3{e: [-0.25, -0.25, 0.0]}, hit.p);
        assert_eq!(Vec3{e: [0.0, 0.0, -1.0]}, hit.normal);
        assert_eq!(0.5, hit.t);
        assert!(hit.front_face);

        assert_eq!(Point3{e: [0.0, 0.0, 0.0]}, miss.p);
        assert_eq!(Vec3{e: [0.0, 0.0, 0.0]}, miss.normal);
        assert_eq!(-1.0, miss.t);
        assert!(!miss.front_face);
    }
}