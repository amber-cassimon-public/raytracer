use std::vec::Vec;
use crate::hit_record::HitRecord;
use crate::hittables::hittable::Hittable;
use crate::interval::Interval;
use crate::ray::Ray;

pub struct HittableList
{
    pub objects: Vec<Box<dyn Hittable>>
}

impl HittableList
{
    pub fn new(object: Box<dyn Hittable>) -> HittableList
    {
        return HittableList{objects: vec![object]};
    }

    pub fn add(self: &mut Self, object: Box<dyn Hittable>) -> ()
    {
        self.objects.push(object);
    }
}

impl Default for HittableList
{
    fn default() -> Self {
        return HittableList{objects: vec![]};
    }
}

impl Hittable for HittableList
{
    fn hit(&self, ray: &Ray, ray_t: &Interval, hit_record: &mut HitRecord) -> bool {
        let mut temp_record = HitRecord::default();
        let mut hit_anything = false;
        let mut closest_so_far = ray_t.max;

        for object in &self.objects
        {
            let t_interval= Interval::new(ray_t.min, closest_so_far);

            if object.hit(ray, &t_interval, &mut temp_record)
            {
                hit_anything = true;
                closest_so_far = temp_record.t;
                *hit_record = temp_record.clone();
            }
        }

        return hit_anything;
    }
}

#[cfg(test)]
mod tests
{
    use std::rc::Rc;
    use crate::{color::Color, materials::lambertian::Lambertian, point3::Point3, hittables::sphere::Sphere, vec3::Vec3};
    use super::*;

    #[test]
    fn test_default()
    {
        let hl =  HittableList::default();

        assert_eq!(0, hl.objects.len());
    }

    #[test]
    fn test_new()
    {
        let sphere: Sphere =  Sphere::new(Point3{e: [0.0, 0.0, 0.0]}, 0.5, Rc::new(Lambertian::from_solid_color(Color{e: [0.1, 0.5, 0.9]})));

        let hl =  HittableList::new(Box::new(sphere));

        assert_eq!(1, hl.objects.len());
    }

    #[test]
    fn test_add()
    {
        let sphere: Sphere =  Sphere::new(Point3{e: [0.0, 0.0, 0.0]}, 0.5, Rc::new(Lambertian::from_solid_color(Color{e: [0.1, 0.5, 0.9]})));
        let mut hl =  HittableList::default();

        assert_eq!(0, hl.objects.len());

        hl.add(Box::new(sphere));

        assert_eq!(1, hl.objects.len());
    }

    #[test]
    fn test_hit()
    {
        let centered_ray = Ray::new(Point3{e: [0.0, 0.0, 1.0]}, Vec3{e: [0.0, 0.0, -1.0]});
        let offset_ray = Ray::new(Point3{e: [-0.5, 0.5, 1.0]}, Vec3{e: [0.0, 0.0, -1.0]});

        let sphere = Sphere::new(Point3::origin(), 0.5, Rc::new(Lambertian::from_solid_color(Color{e: [0.1, 0.5, 0.9]})));
        let t = Interval::new(0.0, 1.0);

        let hittable_list = HittableList::new(Box::new(sphere));

        let mut hit = HitRecord::default();
        let mut miss = HitRecord::default();

        assert_eq!(true, hittable_list.hit(&centered_ray, &t, &mut hit));
        assert_eq!(false, hittable_list.hit(&offset_ray, &t, &mut miss));

        assert_eq!(Point3{e: [0.0, 0.0, 0.5]}, hit.p);
        assert_eq!(Vec3{e: [0.0, 0.0, 1.0]}, hit.normal);
        assert_eq!(0.5, hit.t);
        assert!(hit.front_face);

        assert_eq!(Point3{e: [0.0, 0.0, 0.0]}, miss.p);
        assert_eq!(Vec3{e: [0.0, 0.0, 0.0]}, miss.normal);
        assert_eq!(-1.0, miss.t);
        assert!(!miss.front_face);
    }
}