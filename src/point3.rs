use crate::vec3::Vec3;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Point3
{
    pub e: [f32; 3]
}

impl Point3
{
    pub fn origin() -> Point3
    {
        return Point3{e: [0.0, 0.0, 0.0]};
    }
}

impl From<Vec3> for Point3
{
    fn from(value: Vec3) -> Self {
        return Point3{e: value.e};
    }
}

impl Into<Vec3> for Point3
{
    fn into(self) -> Vec3 {
        return Vec3{e: self.e};
    }
}