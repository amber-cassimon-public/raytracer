function get_performance_data() {
    return [
        {
            build_type: "Release",
            commit_hash: "807ecd936ce4350dc67824932a18561f544c9157",
            image_width: 568,
            image_height: 320,
            samples_per_pixel: 1024,
            ray_count: 676791140,
            render_time_s: 107.203838,
            note: ""
        },
        {
            build_type: "Release",
            commit_hash: "86f1071f22700be85db6da3dc85eefda72c525a6",
            image_width: 568,
            image_height: 320,
            samples_per_pixel: 1024,
            ray_count: 633313078,
            render_time_s: 155.7600307,
            note: "Added support for solid and checkered textures"
        },
        {
            build_type: "Release",
            commit_hash: "4f91e15e1bdc74f21ee0ece29c80ccc977945685",
            image_width: 568,
            image_height: 320,
            samples_per_pixel: 1024,
            ray_count: 633337134,
            render_time_s: 163.0093493,
            note: "Added support for diffuse lighting"
        }
    ];
}

function pixelRate(performance) {
    const pixels = performance.image_width * performance.image_height;
    return pixels / performance.render_time_s;
}

function rayRate(performance) {
    return performance.ray_count / performance.render_time_s;
}