function get_performance_data() {
    return [
        {
            build_type: "Release",
            commit_hash: "75e36653af436f240f91b3d0235cbe2289a5dea0",
            image_width: 512,
            image_height: 512,
            samples_per_pixel: 1024,
            ray_count: 4322644347,
            render_time_s: 1733.2288947,
            note: ""
        },
        {
            build_type: "Release",
            commit_hash: "4916ef996588ba8fa16c02030e5f540f0c4a7250",
            image_width: 512,
            image_height: 512,
            samples_per_pixel: 1024,
            ray_count: 4323012510,
            render_time_s: 1743.2066194,
            note: "Refactored tracing into framebuffer and PNG export."
        },
        {
            build_type: "Release",
            commit_hash: "1ed5b66a83255e2ac948aa5725734ec7a9adc788",
            image_width: 512,
            image_height: 512,
            samples_per_pixel: 1024,
            initial_ray_count: 268435456,
            total_ray_count: 4322946439,
            ray_gen_time_s: 4.8727004,
            render_time_s: 1757.2355806,
            note: "Refactored initial ray generation to separate it out from from actual rendering."
        }

    ];
}

function totalRays(performance) {
    if (typeof performance.total_ray_count !== "undefined") {
        return performance.total_ray_count;
    }
    else
    {
        return performance.ray_count;
    }
}

function rayGenTime(performance) {
    if (typeof performance.ray_gen_time_s !== "undefined") {
        return performance.ray_gen_time_s;
    }
    else
    {
        return 0;
    }
}

function pixelRate(performance) {
    const pixels = performance.image_width * performance.image_height;
    return pixels / performance.render_time_s;
}

function rayRate(performance) {
    return totalRays(performance) / performance.render_time_s;
}
