# Raytracer
Doing [Raytracing in a weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) in Rust.

Event | Timestamp
---|---
Started | `July 4, 2024 at 8:53:35 PM GMT+2`
Finished "Ray Tracing in One Weekend" | `July 11, 2024 at 7:16:00 PM GMT+2`

![A Cornell Box with the left and right walls flipped](res/Cornell_Box.png)

## [10.5 A Scene with Metal Spheres](https://raytracing.github.io/books/RayTracingInOneWeekend.html#metal/ascenewithmetalspheres)
```rust

fn main()
{
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Metal::new(Color{e: [0.8, 0.8, 0.8]}));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}));

    // World
    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    //let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```

![An image of 3 spheres on a plane with an azure background. The outermost spheres have a metallic, very reflective texture while center sphere has a diffuse blue texture. The right reflective sphere has a yellow tint.](res/Ch_10_5_A_Scene_with_Metal_Spheres.png)

## [10.6 Fuzzy Reflections](https://raytracing.github.io/books/RayTracingInOneWeekend.html#metal/fuzzyreflection)

```rust
fn main()
{
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Metal::new(Color{e: [0.8, 0.8, 0.8]}, 0.3));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    // World
    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    //let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an azure background. The outermost spheres have a metallic, very reflective texture while center sphere has a diffuse blue texture. The right reflective sphere has a yellow tint.](res/Ch_10_6_Fuzzy_Reflections.png)

## [11.2 Snell's Law](https://raytracing.github.io/books/RayTracingInOneWeekend.html#dielectrics/snell'slaw)

```rust
fn main()
{
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    // World
    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    //let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```

![An image of 3 spheres on a plane with an azure background. The rightmost sphere has a metallic, reflective texture while center the sphere has a diffuse blue texture. The leftmost sphere refracts the incoming light, creating mirrored transparency](res/Ch_11_2_Snells_Law.png)

## [11.3 - Total Internal Reflection](https://raytracing.github.io/books/RayTracingInOneWeekend.html#dielectrics/totalinternalreflection)
```rust
fn main()
{
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.0 / 1.33));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    // World
    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    //let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an azure background. The outermost spheres have a metallic, very reflective texture while center sphere has a diffuse blue texture. The right reflective sphere has a yellow tint.](res/Ch_11_3_Total_Internal_Reflection.png)


## [11.5 - Modelling a Hollow Glass Sphere](https://raytracing.github.io/books/RayTracingInOneWeekend.html#dielectrics/modelingahollowglasssphere)
```rust
fn main()
{
    // World
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_bubble = Rc::new(Dielectric::new(1.0 / 1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.4, material_bubble)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    // let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an azure background. The left sphere is made of glass and has a hollow core, the center sphere has a diffuse texture and the right sphere is a vague metallic texture.](res/Ch_11_5_Modelling_a_Hollow_Glass_Sphere.png)

## [12.1 - Camera Viewing Geometry](https://raytracing.github.io/books/RayTracingInOneWeekend.html#positionablecamera/cameraviewinggeometry)
```rust
fn main()
{
    // World
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_bubble = Rc::new(Dielectric::new(1.0 / 1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.4, material_bubble)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    // let world = cornell_box();

     let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.focal_length = 1.0;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;
    camera.vfov = 120.0;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an azure background. The left sphere is made of glass and has a hollow core, the center sphere has a diffuse texture and the right sphere is a vague metallic texture.](res/Ch_12_1_Camera_Viewing_Geometry.png)

## [12.2 - Positioning and Orienting the Camera](https://raytracing.github.io/books/RayTracingInOneWeekend.html#positionablecamera/positioningandorientingthecamera)
```rust
fn main()
{
    // World
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_bubble = Rc::new(Dielectric::new(1.0 / 1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.4, material_bubble)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    // let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;
    camera.vfov = 20.0;
    camera.lookfrom = Point3{e: [-2.0, 2.0, 1.0]};
    camera.lookat = Point3{e: [0.0, 0.0, -1.0]};
    camera.vup = Vec3::new(0.0, 1.0, 0.0);

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an yellow-greenish background. The left sphere is made of glass and has a hollow core, the center sphere has a diffuse texture and the right sphere is a vague metallic texture.](res/Ch_12_2_Positioning_and_Orienting_the_Camera.png)

## [13.2 - Generating Sample Rays](https://raytracing.github.io/books/RayTracingInOneWeekend.html#defocusblur/generatingsamplerays)
```rust
fn main()
{
    // World
    let material_ground = Rc::new(Lambertian::new(Color{e: [0.8, 0.8, 0.0]}));
    let material_center = Rc::new(Lambertian::new(Color{e: [0.1, 0.2, 0.5]}));
    let material_left = Rc::new(Dielectric::new(1.5));
    let material_bubble = Rc::new(Dielectric::new(1.0 / 1.5));
    let material_right = Rc::new(Metal::new(Color{e: [0.8, 0.6, 0.2]}, 1.0));

    let mut world = HittableList::default();
    world.add(Box::new(Sphere::new(Point3{e: [0.0, -100.5, -1.0]}, 100.0, material_ground)));
    world.add(Box::new(Sphere::new(Point3{e: [0.0, 0.0, -1.2]}, 0.5, material_center)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.5, material_left)));
    world.add(Box::new(Sphere::new(Point3{e: [-1.0, 0.0, -1.0]}, 0.4, material_bubble)));
    world.add(Box::new(Sphere::new(Point3{e: [1.0, 0.0, -1.0]}, 0.5, material_right)));

    // let world = cornell_box();

    let world_box: Box<dyn Hittable> = Box::new(world);

    let mut camera = Camera::default();
    camera.aspect_ratio = 16.0 / 9.0;
    camera.image_height = 320;
    camera.samples_per_pixel = 1024;
    camera.max_depth = 32;
    camera.vfov = 20.0;
    camera.lookfrom = Point3{e: [-2.0, 2.0, 1.0]};
    camera.lookat = Point3{e: [0.0, 0.0, -1.0]};
    camera.vup = Vec3::new(0.0, 1.0, 0.0);
    camera.defocus_angle = 5.0;
    camera.focus_dist = 3.4;

    let render_info = camera.render(&world_box);

    eprint!("{}", render_info);
}
```
![An image of 3 spheres on a plane with an yellow-greenish background. The left sphere is made of glass and has a hollow core, the center sphere has a diffuse texture and the right sphere is a vague metallic texture.](res/Ch_13_2_Generating_Sample_Rays.png)
